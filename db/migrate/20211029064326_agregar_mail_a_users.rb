class AgregarMailAUsers < ActiveRecord::Migration[6.1]
  def change
  	add_column :users, :dni, :string
  end
  
  add_index :users, :dni, unique: true
  
end

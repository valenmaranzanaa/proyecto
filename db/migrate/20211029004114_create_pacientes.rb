class CreatePacientes < ActiveRecord::Migration[6.1]
  def change
    create_table :pacientes do |t|
      t.string :nombre
      t.string :apellido
      t.string :dni
      t.integer :edad
      t.boolean :vacuna_gripe
      t.boolean :vacuna_fa
      t.boolean :vacuna_covid
      t.boolean :riesgo

      t.timestamps
    end
  end
end

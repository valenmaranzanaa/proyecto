class CreateUsuarios < ActiveRecord::Migration[6.1]
  def change
    create_table :usuarios do |t|
      t.string :usser
      t.string :password
      t.string :token
      t.string :email

      t.timestamps
    end
  end
end
